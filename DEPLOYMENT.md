# Installation de la machine hôte

## Installation de node JS

Node déjà installé via les packages standard, cf [Déploiement de l'an dernier](https://github.com/romulusFR/lifap5-backend-2019-2020/blob/master/DEPLOYMENT.md)

## Installation de MongoDB

Installation en suivant les [instructions officielles](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/), scriptées via Ansible (répertoire `system`).
