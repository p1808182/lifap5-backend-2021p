# Projet LIFAP5 printemps 2021

Ce dépôt contient les éléments pour le projet de LIFAP5 du printemps 2021. En particulier:

- Le code source de l'application côté serveur
- Le code de départ de l'interface à réaliser dans le projet
- Un exemple d'interface complète permettant d'avoir une idée de ce qui est attendu
- Le sujet du projet

