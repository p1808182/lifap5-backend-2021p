/**
 * @file Structures et opérations sur les utilisateurs.
 * @author Emmanuel COQUERY
 */
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({ login: String, apiKey: String });
const User = mongoose.model("User", userSchema);

/**
 * Get a user using his/hers API Key
 * @param {String} apiKey the api key to look for.
 */
const getUserByAPI = (apiKey) => User.findOne({ apiKey }).exec();

module.exports = { User, getUserByAPI };
