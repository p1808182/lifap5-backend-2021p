/**
 * @file Handlers for managing various shared needs
 * @author Emmanuel COQUERY
 */
const { isUUID } = require("validator");
const createError = require("http-errors");
const { logger } = require("./logger");

const { getUserByAPI } = require("./user");

const authByApiKey = (req, res, next) => {
  const token = req.header("x-api-key");
  if (!token || !isUUID(token, 4)) {
    logger.warn(`Wrong api key: '${token}'`);
    const err = new createError.Unauthorized(
      `x-api-key is not provided or invalid`
    );
    return next(err);
  }
  return getUserByAPI(token).then((u) => {
    if (!u) {
      // eslint-disable-next-line promise/no-callback-in-promise
      return next(
        new createError.Unauthorized(`x-api-key "${token}" does not exist`)
      );
    }
    res.locals.user = u;
    // eslint-disable-next-line promise/no-callback-in-promise
    return next();
  });
};

module.exports = { authByApiKey };
