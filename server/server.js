/**
 * @file Server definition: set up HTTP server with express app, establish connections with db,
 * manage errors and log by loading various modules.
 *
 * @author Emmanuel COQUERY
 */

// Libraries
const http = require("http");

// App's modules
const { logger } = require("./logger");
const { app } = require("./app");
const config = require("./config");

const httpServer = http.createServer(app);
httpServer.listen(config.httpPort, () => {
  logger.info("Started http server");
});
