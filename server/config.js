/**
 * @file Application's configuration exposed to other modules as a dictionnary.
 * @author Emmanuel COQUERY
 */
// Libraries

const env = process.env.NODE_ENV || "development";
const debugLvl = process.env.DEV_CONSOLE_DEBUG_LVL || "debug";

const appName = "lifap5";

const httpPort = Number.parseInt(process.env.NODE_PORT, 10) || 3000;

const mongodbUrl = process.env.MONGO_URL || "mongodb://localhost/test";

module.exports = { appName, mongodbUrl, httpPort, env, debugLvl };
