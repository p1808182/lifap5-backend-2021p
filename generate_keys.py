#!/usr/bin/env python3
import csv
import fileinput
import sys
from uuid import uuid4

writer = csv.writer(sys.stdout, delimiter="\t")
for line in fileinput.input():
    writer.writerow([str(line).strip(), str(uuid4())])
